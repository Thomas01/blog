@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="col-12 text-center mt-5 pt-5">
                <h5>Welcome to the blog with great articles, click the button below to see them</h5>
            <br>
            <a href="{{ route('post.index') }}" class="btn btn-outline-primary btn-lg mt-5 p-3">{{ __('Show Posts') }}</a>
        </div>
    </div>
</div>
@endsection
