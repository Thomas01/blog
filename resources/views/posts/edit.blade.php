@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="{{ route('post.index') }}" class="btn btn-outline-primary btn-sm">{{ __('Go Back') }}</a>
                <div class="border rounded mt-5 pl-4 pr-4 pt-4 pb-4">
                    <h5 class="m-2">Edit Post</h5>
                    <hr>

                    @if ($errors->any())
                               <div class="w4/8 m-auto text-center">
                                   @foreach($errors->all() as $error)
                                    <li class="alert alert-danger" role="alert">
                                        {{ $error }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </li>
                                @endforeach
                            </div>
                        @endif
                    <form  method="POST" action="{{ route('post.update', $post->id) }}">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="control-group col-12">
                                <input type="text" id="title" class="form-control" name="title"
                                placeholder="Enter Post Title" value="{{ $post->title }}" required>
                            </div>

                            <div class="control-group col-12 mt-2">
                                <textarea id="body" class="form-control" name="body" placeholder="Enter Post Body"
                                rows="5" required>{{ $post->body }}</textarea>
                            </div>
                        </div>
                   
                        <div class="row mt-2">
                            <div class="control-group col-12 text-center">
                                <button id="btn-submit" class="btn btn-outline-primary btn-sm">
                                {{ __('Update Post') }}
                                </button>
                            </div>
                        </div>
                    
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection