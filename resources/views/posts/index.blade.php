@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">

            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success alert-dismissible fade show" >
                        {{ session('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>

            <div class="col-12 pt-2">
                 <div class="row">
                    <div class="col-8">
                        <h4 class="display-one mb-3">The Blog!</h4>
                        <h5 class="mb-3">Click on any of the topics to read, edit, delete and to create posts click on the button to your right enjoy !!!</h5>
                    </div>
                    <div class="col-4">
                        <a href="{{ route('post.create') }}" class="btn btn-outline-secondary btn float-right">Create Post</a>
                    </div>
                </div>                
                @forelse($posts as $post)
                    <ul >
                        <li><a class="text-decoration-none" href="{{ route('post.show', $post->id) }}">{{ ucfirst($post->body) }}</a></li>
                    </ul>
                @empty
                    <p class="text-warning">No blog Posts available, kindly create by clicking on the create post button </p>
                @endforelse
            </div>
        </div>
    </div>
<script>
  
@endsection