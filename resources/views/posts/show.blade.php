@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                  <a href="{{ route('post.index')}}"class="btn btn-outline-secondary btn-sm">{{ __('Go Back') }}</a>
                  <h5 class="display-one mt-3">{{ ucfirst($post->title) }}</h5>
                  <p>{!! $post->body !!}</p> 
                  <hr>
                @if(Auth::check() && Auth::id() == $post->user_id)
                    <div class="d-flex ">
                      <a href="{{ route('post.edit', $post->id) }}" class="btn btn-outline-secondary mt-0 m-3 btn-sm">{{ __('Edit Post') }}</a>
                      <br><br>
                    <form id="delete-frm" method="POST" action="{{ route('post.destroy', $post->id) }}">
                      @method('DELETE')
                      @csrf
                    <button class="btn btn-outline-danger mt-0 m-3 text-color-white btn-sm">{{ __('Delete Post') }}</button>
                  </form>
                   </div>
                @endif
            </div>
        </div>
    </div>
@endsection