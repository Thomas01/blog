@extends('layouts.app')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                    <a href="{{ route('post.index') }}" class="nav-link active text-black " aria-current="page" href="#"> {{ __('Go Back') }}</a>
                    </div>
                    <div class="card-body">

                        @if ($errors->any())
                               <div class="w4/8 m-auto text-center">
                                   @foreach($errors->all() as $error)
                                    <li class="alert alert-danger" role="alert">
                                        {{ $error }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </li>
                                @endforeach
                            </div>
                        @endif

                        <form method="POST" action="{{ route('post.store') }}">
                            @csrf  
                        <div class="mb-3">
                          <input type="text" class="form-control" value="{{ old('name') }}" name="title" id="title" placeholder="Title">
                        </div>
                        <div class="mb-3">
                        <textarea class="form-control"  name="body" placeholder="what is on your mind ?" id="body" rows="5" autofocus ></textarea>
                        </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-outline-secondary btn">
                                        {{ __('Create Post') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection