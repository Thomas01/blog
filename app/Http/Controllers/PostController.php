<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;

class PostController extends Controller
{
    
    public function index(): View
    {
        $posts = Posts::all(); 

        return view('posts.index', compact('posts')); 
    }

    public function create(): View
    {
        return view('posts.create');
    }

    public function store(PostRequest $request): RedirectResponse    
    {
        if (Auth::check()){

            Posts::create([

              'title' => $request->title,
              'body' => $request->body,
              'user_id' => Auth::user()->id

            ]);

        } else {

            return redirect()->route('login')->with('message', 'Please login or register before you can create a post');
        }

        return redirect()->route('post.index')->with('message', 'Post Created Successfully');
    }

    public function show(Posts $post): View
    {
       return view('posts.show', compact('post'));
    }


    public function edit(Posts $post): View | RedirectResponse 
    {
       if(Auth::check() && Auth::id() == $post->user_id){

        return view('posts.edit', compact('post'));

       } else {

        return redirect()->route('login');
       }
    }

   
    public function update(PostRequest $request, Posts $post): RedirectResponse 
    {
        $post->update($request->validated());

        return redirect()->route('post.index')->with('message', 'Post updated Successfully');

    }

    public function destroy(Posts $post): RedirectResponse 
    {
        $post->delete();

        return redirect()->route('post.index')->with('message', 'Post Deleted Successfully');
    }
}
