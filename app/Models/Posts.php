<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;
class Posts extends Model
{
    use HasFactory;
    use Rateable;
    
    protected $fillable = ['title', 'body', 'user_id'];

    public function rating()
{
  return $this->hasMany(Rating::class);
}
}
